(function (window, Matrix3D){

    //create Scene
    function Scene(){
        this.container = elt("div", "scene");
        this.area = new Area();
    }

    Scene.prototype.addTo = function(container){
        this.area.addTo(this.container);
        body.appendChild(this.container);
    };

    //create Area
    function Area(){
        this.container = elt("div", "area");
        this.flag = false;
        this.buildWalls = new BuildWalls(5, 5, 120, 120);
        this.coordinates = new Coordinates();
    }

    Area.prototype.addTo = function(container){
        this.buildWalls.addTo(this.container);
        container.appendChild(this.container);
    };

    Area.prototype.onMouseDown = function (evt) {
        this.coordinates.clientX = evt.clientX;
        this.coordinates.clientY = evt.clientY;
        this.flag = true;
    };

    Area.prototype.onMouseUp = function (evt) {
        this.flag = false;
    };

    Area.prototype.onMouseMove = function (evt) {
        var rx = (this.coordinates.clientY - evt.clientY)%360;
        var ry = (this.coordinates.clientX - evt.clientX)%360;
        if (this.flag) this.buildWalls.setMatrix(rx, ry)
    };

    //create BuildWalls
    function BuildWalls(x, y, sizeX, sizeY){
        this.container = elt("div", "wrapper");
        this.container.style.width = (x * sizeX) + "px";
        this.container.style.height = (y * sizeY) + "px";
        this.matrix = Matrix3D.create();
        this.walls = [];
        for(var i = 0; i < 6; i++) this.walls.push(new Wall(x, y, sizeX, sizeY));
    }

    BuildWalls.prototype.addTo = function(container){
        this.walls.forEach(function(wall, _){
            wall.addTo(this.container);
        }, this);
        this.walls[0].setMatrix(null, null);
        this.walls[1].setMatrix(null, -90);
        this.walls[2].setMatrix(null, 90);
        this.walls[3].setMatrix(90, null);
        this.walls[4].setMatrix(-90, null);
        this.walls[5].setMatrix(null, 180);
        container.appendChild(this.container);
    };

    BuildWalls.prototype.setMatrix = function(rx, ry){
        // Matrix3D.rotateX(this.matrix, rx);
        // Matrix3D.rotateY(this.matrix, ry);
        // this.container.style.transform = Matrix3D.toTransform3D(this.matrix);
        this.container.style.transform = "rotateY(" +  ry + "deg) rotateX(" + rx + "deg)"
    };

    //create Wall
    function Wall(x, y, sizeX, sizeY){
        this.container = elt("div", "wall");
        this.container.style.width = (x * sizeX) + "px";
        this.container.style.height = (y * sizeY) + "px";
        this.x = x;
        this.y = y;
        this.sizeX = sizeX;
        this.sizeY = sizeY;
        this.girds = [];
        for(var i = 0; i < x * y; i++) this.girds.push(new Gird());
        this.matrix = Matrix3D.create();
    }

    Wall.prototype.addTo = function(container){
        var translateX,
            translateY;
        this.girds.forEach(function(gird, index){
            gird.addTo(this.container);
            gird.setSize(this.sizeX, this.sizeY);
            translateX = (this.sizeX * (index % this.x));
            translateY = (this.sizeY * Math.floor(index / this.y));
            gird.setMatrix(translateX,translateY);
        }, this);
        container.appendChild(this.container);
    };

    Wall.prototype.setMatrix = function(rx, ry){
        rx == null || Matrix3D.rotateX(this.matrix, rx) ;
        ry == null || Matrix3D.rotateY(this.matrix, ry) ;
        Matrix3D.translateZ(this.matrix, -80) ;
        this.container.style.transform = Matrix3D.toTransform3D(this.matrix);
        this.container.style.transformOrigin = "50% 50% 300px";
    };

    //create Gird
    function Gird(){
        this.container = elt("div", "grid");
        this.sizeX = 0;
        this.sizeY = 0;
        this.matrix = Matrix3D.create();
    }

    Gird.prototype.addTo = function(container){
        container.appendChild(this.container);
    };

    Gird.prototype.setSize = function(sizeX, sizeY){
        this.sizeX = sizeX;
        this.sizeY = sizeY;
        this.container.style.width = sizeX + "px";
        this.container.style.height = sizeY + "px";
    };

    Gird.prototype.setMatrix = function(tx,ty){
        Matrix3D.translate(this.matrix, tx, ty , null);
        this.container.style.transform = Matrix3D.toTransform3D(this.matrix);
    };

    var scene = new Scene();
    var body = document.getElementsByClassName("container")[0];
    scene.addTo(body);
    window.addEventListener("mousemove", function (evt) {
        scene.area.onMouseMove(evt)
    });
    window.addEventListener("mousedown", function (evt) {
        scene.area.onMouseDown(evt)
    });
    window.addEventListener("mouseup", function (evt) {
        scene.area.onMouseUp(evt)
    });

    //create coordinates
    function Coordinates() {
        this.clientX = 0;
        this.clientY = 0;
    }

    //Helpers
    function CreateElement(){}

    CreateElement.prototype.addTo = function(container){
        container.appendChild(this.container);
    };

    function elt(name, className) {
        var elt = document.createElement(name);
        if (className) elt.className = className;
        return elt;
    }

})(window, Matrix3D);